package com.cyprojects.broadcastpractice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendPersonDataFragment extends Fragment
{
    private Person person = PersonGenerator.generatePerson( (int) ( Math.random() * 99 ) );
    public static final String PERSON_DATA_RECEIVER_ACTION_NAME = "com.cyprojects.broadcastPractice.PersonDataReceiver";

    @BindView( R.id.nameView ) TextView nameView;
    @BindView( R.id.emailView ) TextView emailView;
    @BindView( R.id.phonenumberView ) TextView phonenumberView;
    @BindView( R.id.avatarImageView ) ImageView avatarImageView;

    public SendPersonDataFragment()
    {

    }

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_send_person_data, container, false );
        ButterKnife.bind( this, view );
        updateView();

        return view;
    }

    public void updateView()
    {
        nameView.setText( person.getFullname() );
        emailView.setText( person.getEmail() );
        phonenumberView.setText( person.getPhonenumber() );
        Glide.with( this ).load( person.getAvatarURL() ).into( avatarImageView );
    }

    @OnClick( R.id.sendButton )
    public void sendPersonData()
    {
        Intent intent = new Intent( PERSON_DATA_RECEIVER_ACTION_NAME );
        intent.putExtra( "PersonData", person );
        getContext().sendBroadcast( intent );
    }
}

