package com.cyprojects.broadcastpractice;

import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable
{
    private Integer personID;
    private String fullname;
    private String email;
    private String phonenumber;
    private String avatarURL;

    /*******************************************/
    public Person( int personID, String fullname, String email, String phonenumber, String avatarURL )
    {
        this.personID = personID;
        this.fullname = fullname;
        this.email = email;
        this.phonenumber = phonenumber;
        this.avatarURL = avatarURL;
    }

    public Person()
    {
    }

    /*******************************************/
    public String getFullname()
    {
        return fullname;
    }

    public void setFullname( String fullname )
    {
        this.fullname = fullname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail( String email )
    {
        this.email = email;
    }

    public String getPhonenumber()
    {
        return phonenumber;
    }

    public void setPhonenumber( String phonenumber )
    {
        this.phonenumber = phonenumber;
    }

    public String getAvatarURL()
    {
        return avatarURL;
    }

    public void setAvatarURL( String avatarURL )
    {
        this.avatarURL = avatarURL;
    }

    public int getPersonID()
    {
        return personID;
    }

    public void setPersonID( int personID )
    {
        this.personID = personID;
    }

    protected Person( Parcel in )
    {
        personID = in.readByte() == 0x00 ? null : in.readInt();
        fullname = in.readString();
        email = in.readString();
        phonenumber = in.readString();
        avatarURL = in.readString();
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        if( personID == null )
        {
            dest.writeByte( (byte) ( 0x00 ) );
        }
        else
        {
            dest.writeByte( (byte) ( 0x01 ) );
            dest.writeInt( personID );
        }

        dest.writeString( fullname );
        dest.writeString( email );
        dest.writeString( phonenumber );
        dest.writeString( avatarURL );
    }

    @SuppressWarnings( "unused" ) public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>()
    {
        @Override
        public Person createFromParcel( Parcel in )
        {
            return new Person( in );
        }

        @Override
        public Person[] newArray( int size )
        {
            return new Person[size];
        }
    };
}