package com.cyprojects.broadcastpractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        getSupportFragmentManager().beginTransaction().replace( R.id.fragment1Container, new SendPersonDataFragment() ).commit();
        getSupportFragmentManager().beginTransaction().replace( R.id.fragment2Container, new ReceivePersonDataFragment() ).commit();
    }
}

