package com.cyprojects.broadcastpractice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.cyprojects.broadcastpractice.SendPersonDataFragment.PERSON_DATA_RECEIVER_ACTION_NAME;

public class ReceivePersonDataFragment extends Fragment
{
    private PersonDataReceiver personDataReceiver = new PersonDataReceiver();

    @BindView( R.id.nameView ) TextView nameView;
    @BindView( R.id.emailView ) TextView emailView;
    @BindView( R.id.phonenumberView ) TextView phonenumberView;
    @BindView( R.id.avatarImageView ) ImageView avatarImageView;

    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
    {
        View view = inflater.inflate( R.layout.fragment_receive_person_data, container, false );
        ButterKnife.bind( this, view );
        getContext().registerReceiver( personDataReceiver, new IntentFilter( PERSON_DATA_RECEIVER_ACTION_NAME ) );

        return view;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        getContext().unregisterReceiver( personDataReceiver );
    }

    public void updateView( Person person )
    {
        nameView.setText( person.getFullname() );
        emailView.setText( person.getEmail() );
        phonenumberView.setText( person.getPhonenumber() );
        Glide.with( this ).load( person.getAvatarURL() ).into( avatarImageView );
    }

    public class PersonDataReceiver extends BroadcastReceiver
    {
        @Override
        public void onReceive( Context context, Intent intent )
        {
            Person person = intent.getParcelableExtra( "PersonData" );

            updateView( person );
        }
    }
}
